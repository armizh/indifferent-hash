Gem::Specification.new do |s|
  s.name      = 'indifferent-hash'
  s.summary   = 'Hashes that whose values can be accessed by a string key or a symbol one indifferently'
  s.version   = '0.1.0'
  s.license   = 'BSD-2-Clause'
  s.authors   = ['Felipe Cabrera']
  s.email     = 'fecabrera0@outlook.com'
  s.files     = `git ls-files lib`.split(/\n/)
  s.homepage  = 'https://gitlab.com/armizh/indifferent-hash'
end