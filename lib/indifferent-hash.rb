class IndifferentHash < Hash
  def [](key)
    super(key.to_s)
  end

  def []=(key, value)
    super(key.to_s, value)
  end

  def except(*keys)
    dup.except!(*keys)
  end

  def except!(*keys)
    keys.each { |key| delete(key) }
    self
  end

  def delete(key)
    super(key.to_s)
  end
  
  def self.from_hash(source, dest = IndifferentHash.new)
    source.each do |key, value|
      case value
      when Hash
        dest[key] = from_hash(value)
      else
        dest[key] = value
      end
    end

    dest
  end
end